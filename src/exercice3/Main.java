package exercice3;

public class Main {

	public static void main(String[] args) {

		System.out.println("Please select the right answer. \n");

		var question1 = new Question("What is your name?", 10);
		Reponse reponse = new Reponse("Damien", false);
		Reponse reponse1 = new Reponse("Dupont", true);
		Reponse reponse2 = new Reponse("Martin", false);
		question1.addReponse(reponse);
		question1.addReponse(reponse1);
		question1.addReponse(reponse2);
		
		var question2 = new Question("Where are you from?", 50);
		Reponse reponse3 = new Reponse("Iran", false);
		Reponse reponse4 = new Reponse("France", true);
		Reponse reponse5 = new Reponse("Japan", false);
		Reponse reponse6 = new Reponse("Trukey", false);
		question2.addReponse(reponse3);
		question2.addReponse(reponse4);
		question2.addReponse(reponse5);
		question2.addReponse(reponse6);

		question1.afficher();
		question2.Afficher();

	}

}

package exercice3;

public class Reponse {
	
	private String reponse;
	boolean bonneReponse;

	Reponse(String reponse, boolean bonneReponse){
		
		this.setReponse(reponse);
		this.bonneReponse = bonneReponse;
	}
	public String getReponse(){
		return reponse;
	}
	public void setReponse(String reponse) {
		this.reponse = reponse;
	}
}

package exercice3;

import java.util.ArrayList;

public class Question extends sujet{
		
	private String enonce;
	private int difficulte = 50;
	
	ArrayList<Reponse> reponse = new ArrayList<>();
	ArrayList<Boolean> bonneReponse = new ArrayList<>();
	
	protected void addReponse (Reponse reponse) {
		this.reponse.add(reponse);
	}
	
	Question(String enonce, int difficulte){
		this.setDifficulte(difficulte);
		this.enonce = enonce;
		System.out.println(enonce);
	}

	int getDifficulte() {
		return this.difficulte;
	}
	
	String getEnonce() {
		return this.enonce;
	}
	
	void setDifficulte (int difficulte) {
		this.difficulte = difficulte;
	}
	public String toString() {
		String reponse = "";
		for (int i = 0; i < this.reponse.size(); i++) {
		reponse = reponse + this.reponse.get(i).getReponse()+"\n";}
		return reponse;
	}
	
	public void Afficher () {
		System.out.println(toString());
		//JOptionPane.showMessageDialog(null, this.toString(),this.getEnonce(), JOptionPane.INFORMATION_MESSAGE);
	}
}

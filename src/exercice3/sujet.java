package exercice3;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class sujet{

	int difficulteQuestion = 0;
	int difficulteMoyenTotal;

	ArrayList<Question> listDeQuestions = new ArrayList<Question>();


	public ArrayList<Question> getQuestions() {
		return listDeQuestions;
	}

	public void setQuestions(ArrayList<Question> listDeQuestions) {
		this.listDeQuestions = listDeQuestions;
	}
	public void addQuestion(Question listDeQuestions) {
		this.listDeQuestions.add(listDeQuestions);		
	}

	public void difficulte() {

		for (int i = 0; i<this.listDeQuestions.size(); i++) {
			difficulteQuestion = difficulteQuestion + this.listDeQuestions.get(i).getDifficulte();
		}
		difficulteMoyenTotal = difficulteQuestion/(this.listDeQuestions.size());
		JOptionPane.showMessageDialog(null, "La difficulté moyen des questions est: " + difficulteMoyenTotal);
	}


	@Override
	public String toString() {
		String temp = "";
		for (int i = 0; i < this.listDeQuestions.size(); i++) {
			temp = temp + this.listDeQuestions.get(i).getEnonce()+"\n";
		}
		return temp;
	}

	public void afficher() {
		System.out.println(toString());
		for (int i = 0; i < this.listDeQuestions.size(); i++) {
			this.listDeQuestions.get(i).Afficher();
		}
	}
}
package exercice4;

public class Patients{

	String nom;
	String prenom;
	Dates dateDeNaissance;
	Sexe sexe;

	Patients(String nom, String prenom, Dates dateDeNaissance, Sexe sexe){
		
		this.nom = nom.toUpperCase();
		this.prenom = prenom;
		this.dateDeNaissance = dateDeNaissance;
		this.sexe = sexe;

	}
	public String toString () {
		return "Nom: " + nom + "\nPrenom: " + prenom + "\nDate de naissance: " + dateDeNaissance + "\nSexe: " + sexe;
	}
	
	
}
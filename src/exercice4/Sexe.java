package exercice4;

public class Sexe {
	
	String sexe;
	
	Sexe(String sexe){
		
		if (sexe == "f" || sexe == "féminin" || sexe == "feminin") {
			this.sexe = "feminin";
		}
		else if (sexe == "m" || sexe == "masculin") {
			this.sexe = "masculin";
		}
		else {
			this.sexe = "Invalide!!!";
		}
	}
	public String toString () {
		return sexe;
	}
}

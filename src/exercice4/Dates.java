package exercice4;

import java.time.LocalDate;

public class Dates{
	
	int year;
	int month;
	int day;
	
	Dates(int year, int month, int day) {
		LocalDate date = LocalDate.of(year, month, day);
		this.year = year;
		this.month = month;
		this.day = day;
	}
	
//	public LesDates now()
//	{
//		return this.now();
//	}
	
	public String toString() {
		return year + "/" + month + "/"+ day;
	}
}

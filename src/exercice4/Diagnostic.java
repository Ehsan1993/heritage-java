package exercice4;

import java.util.Calendar;

public class Diagnostic {

	Calendar calendar = Calendar.getInstance();
	String soignant;
	Dates dateDeTraitement;
	Maladie maladie;
	int amelioration; //amélioration en pourcentage
	Medicaments medicament;

	Diagnostic(String soignant, Dates dateDeTraitement, Maladie maladie, int amelioration){

		this.soignant = soignant;
		this.dateDeTraitement = dateDeTraitement;
		this.maladie = maladie;
		this.amelioration = amelioration;
	}	
	
	public String toString() {
		return "Soignant: " 
				+ soignant + "\nDate de traitement: " + dateDeTraitement + "\nType de maladie: " 
				+ maladie + "\nAmelioration: " + amelioration + " %\n";
	}
}

package exercice4;

public class Main {

	public static void main(String[] args) {
		
		var dateDeNaissanceMartin = new Dates(1985, 12, 25);
		var sexePatient1 = new Sexe("féminin");
		var Martin = new Patients("Sonnfraud", "Martin", dateDeNaissanceMartin, sexePatient1);

		System.out.println(Martin);
		var dateDeTraitementDupont = new Dates(2013, 11, 27);
		var diagnosticMartin = new Diagnostic("Dr. Dupont", dateDeTraitementDupont, Maladie.HEADACHE, 10);
		System.out.println(diagnosticMartin);
		
		
		var maladieMartin = new Medicaments (Maladie.HEADACHE);
		maladieMartin.toString();
	}

}

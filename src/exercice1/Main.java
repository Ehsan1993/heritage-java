package exercice1;

public class Main {

	public static void main(String[] args) {

		var Dupont = new Employe("Dupont", 35, 2000);
		var Justine = new Employe("Justine", 30, 2100);
		Dupont.afficher();
		Justine.afficher();
		
		Dupont.augmentation(500);
		Justine.augmentation(500);
		
		Dupont.afficher();
		Justine.afficher();

		var technicien1 = new Technicien("Martin", 45, 2500, 2);
		var technicien2 = new Technicien("Albert", 30, 2400, 3);
		
		technicien1.afficher();
		technicien2.afficher();
		
		technicien1.augmentation(300);
		technicien2.augmentation(500);
		
		technicien1.afficher();
		technicien2.afficher();

	}
}

package exercice1;

public class Technicien extends Employe {

	int grade;
	int prime;
	
	Technicien(String nom, int age, double salaire, int grade) {
		super(nom, age, salaire);
		this.grade = grade;
	}

	int prime(){
		
		if (this.grade == 1) {
			prime = 100;
		}
		else if (this.grade == 2) {
			prime = 200;
		}
		else if (this.grade == 3) {
			prime = 300;
		}
		return prime;
	}
	
	@Override
	public double calculeSalaire() {
		return this.salaire + prime();
	}
	
	
	@Override
	public String toString () {
		return "Nom de salarié: " + this.nom + "\n" + "Age: " + this.Age + " ans \n" + "Salaire: " + this.salaire + " $ + Prime: " + prime() + " $ = " + calculeSalaire() + " $\n";
	}
}

package exercice1;
import javax.swing.JOptionPane;

public class Employe {

	String nom;
	int Age;
	double salaire;

	Employe(String nom, int age, double salaire){
		this.nom = nom;
		this.Age = age;
		this.setSalaire(salaire);
	}

	public void setSalaire(double salaire) {
		this.salaire = salaire;
	}

	public void augmentation(double augmentation) {
		
		double pourcentage = augmentation/salaire*100;
		setSalaire(salaire+augmentation);
		
		JOptionPane.showMessageDialog(null, "Le salaire est augmenté de: " + pourcentage + " % ", "L'Augmentation de salaire de " + nom, JOptionPane.INFORMATION_MESSAGE);
	}

	public String toString() {
		return "Nom de salarié: " + this.nom + "\n" + "Age: " + this.Age + " ans \n" + "Salaire: \t" + calculeSalaire() + " $\n";
	}

	public void afficher() {
		JOptionPane.showMessageDialog(null, this.toString(), nom, JOptionPane.INFORMATION_MESSAGE);
	}


	public double calculeSalaire() {
		return salaire;
	}
}

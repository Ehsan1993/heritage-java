package exercice5;

public class Personne {

	String nom;
	String prenom;
	int compteBancaire; // montant de sont compte en euros
	
	Personne(String nom, String prenom){
		this.nom = nom;
		this.prenom = prenom;
		this.compteBancaire = 0;
	}
	void addSous(int montant) {
		this.compteBancaire = this.compteBancaire + montant;
	}
}

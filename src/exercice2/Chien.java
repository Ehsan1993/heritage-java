package exercice2;

import javax.swing.JOptionPane;

public class Chien extends Animal {

	String race;

	public Chien(String nom, String espece, int age, String race) {
		super(nom, espece, age);
		this.setRace(race);
	}
	//Ajoute de getter et setter pour les variables nécessaires
	public String getRace() {
		return race;
	}
	public void setRace(String race) {
		this.race = race;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public void setEspece(String espece) {
		this.espece = espece;
	}
	
	
	@Override
	void dormir() {
		JOptionPane.showMessageDialog(null, "Je dors dans une niche.", "Chien : " + nom, JOptionPane.INFORMATION_MESSAGE);
	}

	void aboyer() {
		JOptionPane.showMessageDialog(null, "J'aboie", "Chien : " + nom, JOptionPane.INFORMATION_MESSAGE);
	}
	public String toString() {
		return "Je m'appelle " + nom + ", j'ai " + age + " ans et je suis un " + espece + " et je suis de race " + race;
	}
}

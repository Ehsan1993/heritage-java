package exercice2;

public abstract class  Animal {

	String nom;
	String espece;
	int age;

	public Animal(String nom, String espece, int age){
		this.nom = nom;
		this.espece = espece;
		this.age = age;
	}
	void manger(String nourriture) {
	}

	abstract void dormir();

	public String toString() {
		return "Je m'appelle " + nom + ", j'ai " + age + " ans et je suis un " + espece;
	}
}
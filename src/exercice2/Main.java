package exercice2;

import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {

		//instancier la classe Chien et ses methodes
		var chien1 = new Chien("Tony", "Poney chien", 5, "Shetland");
		JOptionPane.showMessageDialog(null, chien1);
		chien1.aboyer();
		chien1.dormir();

		// pour changer l'age du chien
		chien1.setAge(6);
		JOptionPane.showMessageDialog(null, chien1);

		//instancier la classe Oiseau et ses methodes
		var oiseau1 = new Oiseau("oiseau", "Psittacidae", 3, 100);
		JOptionPane.showMessageDialog(null, oiseau1);
		oiseau1.dormir();
		oiseau1.manger("grain");
		oiseau1.senvoler();

	}

}

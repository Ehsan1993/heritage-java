package exercice2;

import javax.swing.JOptionPane;

public class Oiseau extends Animal {

	int nombreDePlumes;
	
	public Oiseau(String nom, String espece, int age, int nombreDePlumes) {
		super(nom, espece, age);
		this.setNombreDePlumes(nombreDePlumes);
	}

	// Définir les setter et getter nécessaires pour les variables
	public void setNom(String nom) {
		this.nom = nom;
	}
	public void setEspece(String espece) {
		this.espece = espece;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getNombreDePlumes() {
		return nombreDePlumes;
	}
	public void setNombreDePlumes(int nombreDePlumes) {
		this.nombreDePlumes = nombreDePlumes;
	}
	
	
	void senvoler() {
		JOptionPane.showMessageDialog(null, "Je vole", "Oiseau : " + nom, JOptionPane.INFORMATION_MESSAGE);
	}
	void manger(String nourriture) {
		JOptionPane.showMessageDialog(null, "Je ne mange que " + nourriture, "Oiseau : " + nom, JOptionPane.INFORMATION_MESSAGE);
		
	}
	@Override
	void dormir() {
		JOptionPane.showMessageDialog(null, "Je dors dans un nid", "Oiseau : " + nom, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public String toString() {
		return "Je m'appelle " + nom + ", j'ai " + age + " ans et je suis un " + espece + " et j'ai " + nombreDePlumes + " plumes";
	}
}
